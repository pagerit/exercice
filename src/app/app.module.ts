import {DEFAULT_CURRENCY_CODE, LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from "@angular/material/toolbar";
import {ExercicesModule} from "./exercices/exercices.module";
import {HttpClientModule} from "@angular/common/http";
import localeFr from '@angular/common/locales/fr';
import localeEN from '@angular/common/locales/en';
import {registerLocaleData} from "@angular/common";
import {MatExpansionModule} from "@angular/material/expansion";
registerLocaleData(localeFr)
registerLocaleData(localeEN)

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ExercicesModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    HttpClientModule,
    MatExpansionModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'fr-FR'},
    { provide: DEFAULT_CURRENCY_CODE, useValue: 'EUR' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
