import {Injectable} from '@angular/core';
import {BehaviorSubject, filter, map, mergeMap, Observable, of, scan, Subject} from "rxjs";

export type Utilisateur = {
  prenom: string
  age: number
  hero: string
  couleur: string
}


const utilisateurs: Utilisateur[] = [
  {
    prenom: 'Emma',
    age: 22,
    hero: 'Miss Marvel',
    couleur: 'green'
  },
  {
    prenom: 'Alice',
    age: 56,
    hero: 'Percy',
    couleur: 'red'
  },
  {
    prenom: 'Félix',
    age: 31,
    hero: 'Charles Leclerc',
    couleur: 'blue'
  },
  {
    prenom: 'Nathan',
    age: 42,
    hero: 'Iron Man',
    couleur: 'green'
  },
  {
    prenom: 'Logan',
    age: 18,
    hero: 'Bob l\'éponge',
    couleur: 'green'
  },
  {
    prenom: 'Jacob',
    age: 25,
    hero: 'Eminem',
    couleur: 'red'
  },
  {
    prenom: 'Amélia',
    age: 16,
    hero: 'Aldous Huxley',
    couleur: 'blue'
  },
  {
    prenom: 'Charlotte',
    age: 23,
    hero: 'David Douillet',
    couleur: 'green'
  },
  {
    prenom: 'Noah',
    age: 70,
    hero: 'Forest Gump',
    couleur: 'red'
  },
  {
    prenom: 'William',
    age: 68,
    hero: 'Iron Man',
    couleur: 'red'
  },
  {
    prenom: 'Sarah',
    age: 53,
    hero: 'Forest Gump',
    couleur: 'red'
  }
]

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {
  utilisateursFiltre: BehaviorSubject<Utilisateur[]> = new BehaviorSubject(utilisateurs)

  addUser = (utilisateurs: Utilisateur) => {
    this.utilisateursFiltre.next([
      ...this.utilisateursFiltre.value,
      utilisateurs
    ])
  }

  deleteUser = (utilisateur: Utilisateur) => {
    const users = [ ...this.utilisateursFiltre.value]
    users.splice(users.findIndex(u => u === utilisateur), 1)
    this.utilisateursFiltre.next(users)
  }
}
