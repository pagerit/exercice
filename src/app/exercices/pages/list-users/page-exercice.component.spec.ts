import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageExerciceComponent } from './page-exercice.component';

describe('ListUsersComponent', () => {
  let component: PageExerciceComponent;
  let fixture: ComponentFixture<PageExerciceComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PageExerciceComponent]
    });
    fixture = TestBed.createComponent(PageExerciceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
