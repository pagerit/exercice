import { Component } from '@angular/core';
import {UtilisateurService, Utilisateur} from "../../services/utilisateur.service";
import {MatDialog} from "@angular/material/dialog";
import {FormulaireComponent} from "../../components/formulaire/formulaire.component";

@Component({
  selector: 'app-list-users',
  templateUrl: './page-exercice.component.html',
  styleUrls: ['./page-exercice.component.scss']
})
export class PageExerciceComponent {
  datasource = this.utilisateurService.utilisateursFiltre
  displayedColumns: (keyof Utilisateur | 'delete')[] = ['prenom', 'age', 'couleur', 'hero', 'delete'];

  constructor(
    private utilisateurService: UtilisateurService,
    private dialog: MatDialog
  ) {

  }
  openModal = () => {
    this.dialog.open(FormulaireComponent);
  }
  deleteUser = (utilisateur: Utilisateur) => {
    this.utilisateurService.deleteUser(utilisateur)
  }
}
