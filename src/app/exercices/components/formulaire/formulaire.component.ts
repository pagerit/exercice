import {Component} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {UtilisateurService} from "../../services/utilisateur.service";

@Component({
  selector: 'app-formulaire',
  templateUrl: './formulaire.component.html',
  styleUrls: ['./formulaire.component.scss']
})
export class FormulaireComponent {
  result: string = ''

  userForm = this.fb.nonNullable.group({
    prenom: ['', [Validators.required] ],
    age: [20, [Validators.required] ],
    hero: ['', [Validators.required] ],
    couleur: ['green', Validators.required ]
  });

  constructor(
    private dialogRef: MatDialogRef<FormulaireComponent>,
    private saveDataService: UtilisateurService,
    private fb: FormBuilder)  {}

  close = () => {
    this.dialogRef.close()
  }

  submit = () => {
    if (this.userForm.valid) {
      const utilisateur = this.userForm.getRawValue()
      this.saveDataService.addUser(utilisateur)
      this.dialogRef.close()
    }
  }
}
