import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PageExerciceComponent} from "./exercices/pages/list-users/page-exercice.component";

const routes: Routes = [
  { path: 'exercice', component: PageExerciceComponent },
  { path: '**', redirectTo: '/exercice'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
